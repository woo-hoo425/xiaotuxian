"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "install",
  setup(__props) {
    let installList = common_vendor.ref(["授权管理", "问题反馈", "联系我们"]);
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(common_vendor.unref(installList), (item, index, i0) => {
          return {
            a: common_vendor.t(item),
            b: index
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-11e00aa7"], ["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/pages/install/install.vue"]]);
wx.createPage(MiniProgramPage);
