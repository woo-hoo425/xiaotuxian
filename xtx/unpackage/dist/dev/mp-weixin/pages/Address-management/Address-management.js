"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_api_getAddress = require("../../utils/api/getAddress.js");
require("../../utils/http.js");
if (!Array) {
  const _easycom_uni_group2 = common_vendor.resolveComponent("uni-group");
  _easycom_uni_group2();
}
const _easycom_uni_group = () => "../../uni_modules/uni-group/components/uni-group/uni-group.js";
if (!Math) {
  _easycom_uni_group();
}
const _sfc_main = {
  __name: "Address-management",
  setup(__props) {
    let goRemoveAdress = () => {
      common_vendor.index.navigateTo({
        url: "/pages/Address-modules/Address-modules"
      });
    };
    let list = async () => {
      let res = await utils_api_getAddress.getAddress();
      console.log(res);
    };
    let addAdress = () => {
      common_vendor.index.navigateTo({
        url: "/pages/Address-modules/Address-modules"
      });
    };
    common_vendor.onLoad(() => {
      list();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o((...args) => common_vendor.unref(goRemoveAdress) && common_vendor.unref(goRemoveAdress)(...args)),
        b: common_vendor.o((...args) => common_vendor.unref(addAdress) && common_vendor.unref(addAdress)(...args))
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/USER/Desktop/新建文件夹/xiaotuxian/xtx/pages/Address-management/Address-management.vue"]]);
wx.createPage(MiniProgramPage);
