"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_api_user = require("../../utils/api/user.js");
require("../../utils/http.js");
const _sfc_main = {
  __name: "login",
  setup(__props) {
    let app = getApp();
    let getPhoneNumber = async () => {
      let res = await utils_api_user.login("18032746917");
      console.log(res);
      app.globalData.token = res.result.token;
      if (res.code == "1") {
        common_vendor.index.showToast({
          title: res.msg,
          icon: "success"
        });
        setTimeout(() => {
          common_vendor.index.switchTab({
            url: "/pages/my/my"
          });
        }, 500);
      } else {
        common_vendor.index.showToast({
          title: "登录失败",
          icon: "error"
        });
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o((...args) => common_vendor.unref(getPhoneNumber) && common_vendor.unref(getPhoneNumber)(...args))
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/USER/Desktop/新建文件夹/xiaotuxian/xtx/pages/login/login.vue"]]);
wx.createPage(MiniProgramPage);
