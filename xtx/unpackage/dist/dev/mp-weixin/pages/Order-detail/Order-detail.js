"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "Order-detail",
  setup(__props) {
    let goAdressManagement = () => {
      common_vendor.index.navigateTo({
        url: "/pages/Address-management/Address-management"
      });
    };
    common_vendor.onLoad(() => {
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o((...args) => common_vendor.unref(goAdressManagement) && common_vendor.unref(goAdressManagement)(...args)),
        b: common_vendor.o((...args) => _ctx.goOrderList && _ctx.goOrderList(...args))
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/USER/Desktop/新建文件夹/xiaotuxian/xtx/pages/Order-detail/Order-detail.vue"]]);
wx.createPage(MiniProgramPage);
