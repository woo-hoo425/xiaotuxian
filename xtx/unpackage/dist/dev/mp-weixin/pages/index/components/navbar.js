"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  __name: "navbar",
  setup(__props) {
    let { safeAreaInsets } = common_vendor.index.getSystemInfoSync();
    return (_ctx, _cache) => {
      var _a;
      return {
        a: ((_a = common_vendor.unref(safeAreaInsets)) == null ? void 0 : _a.top) + "px"
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-020bfd58"], ["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/pages/index/components/navbar.vue"]]);
wx.createComponent(Component);
