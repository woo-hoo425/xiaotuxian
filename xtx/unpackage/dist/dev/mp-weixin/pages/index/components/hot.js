"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  __name: "hot",
  props: ["hotList"],
  setup(__props) {
    const props = __props;
    console.log(props);
    let {
      hotList
    } = common_vendor.toRefs(props);
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(common_vendor.unref(hotList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.title),
            b: common_vendor.t(item.alt),
            c: common_vendor.f(item.pictures, (item2, index, i1) => {
              return {
                a: item2,
                b: index
              };
            }),
            d: item.id
          };
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/USER/Desktop/新建文件夹/xiaotuxian/xtx/pages/index/components/hot.vue"]]);
wx.createComponent(Component);
