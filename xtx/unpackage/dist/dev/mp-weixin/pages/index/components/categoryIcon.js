"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  __name: "categoryIcon",
  props: {
    categoryList: {
      type: Array,
      default: () => []
    }
  },
  setup(__props) {
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(__props.categoryList, (item, index, i0) => {
          return {
            a: item.icon,
            b: common_vendor.t(item.name),
            c: item.id
          };
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-65a7f6b8"], ["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/pages/index/components/categoryIcon.vue"]]);
wx.createComponent(Component);
