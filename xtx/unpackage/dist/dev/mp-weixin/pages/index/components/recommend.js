"use strict";
const common_vendor = require("../../../common/vendor.js");
const _sfc_main = {
  __name: "recommend",
  props: {
    list: {
      type: Object,
      default: () => {
      }
    }
  },
  setup(__props) {
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(__props.list.title),
        b: common_vendor.t(__props.list.alt),
        c: common_vendor.f(__props.list.pictures, (item, k0, i0) => {
          return {
            a: item,
            b: item.id
          };
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-81bb5221"], ["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/pages/index/components/recommend.vue"]]);
wx.createComponent(Component);
