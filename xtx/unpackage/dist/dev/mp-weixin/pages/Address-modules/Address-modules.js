"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_api_addAddress = require("../../utils/api/addAddress.js");
require("../../utils/http.js");
if (!Array) {
  const _easycom_uni_data_picker2 = common_vendor.resolveComponent("uni-data-picker");
  const _easycom_uni_group2 = common_vendor.resolveComponent("uni-group");
  (_easycom_uni_data_picker2 + _easycom_uni_group2)();
}
const _easycom_uni_data_picker = () => "../../uni_modules/uni-data-picker/components/uni-data-picker/uni-data-picker.js";
const _easycom_uni_group = () => "../../uni_modules/uni-group/components/uni-group/uni-group.js";
if (!Math) {
  (_easycom_uni_data_picker + _easycom_uni_group)();
}
const _sfc_main = {
  __name: "Address-modules",
  setup(__props) {
    common_vendor.wx$1.cloud.init();
    common_vendor.wx$1.cloud.database();
    let options = common_vendor.ref({
      "receiver": "",
      "contact": "",
      "provinceCode": "",
      "cityCode": "",
      "countyCode": "",
      "address": "",
      "postalCode": "",
      "addressTags": "",
      "isDefault": 0
    });
    let list = async () => {
      let res = await utils_api_addAddress.addAddress(options.value);
      console.log(res);
    };
    let onchange = (e) => {
      e.detail.value;
    };
    let onnodeclick = (node) => {
    };
    common_vendor.onLoad(() => {
      list();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.unref(options).receiver,
        b: common_vendor.o(($event) => common_vendor.unref(options).receiver = $event.detail.value),
        c: common_vendor.unref(options).contact,
        d: common_vendor.o(($event) => common_vendor.unref(options).contact = $event.detail.value),
        e: common_vendor.o(common_vendor.unref(onchange)),
        f: common_vendor.o(common_vendor.unref(onnodeclick)),
        g: common_vendor.p({
          placeholder: "请选择地址",
          ["popup-title"]: "请选择城市",
          collection: "opendb-city-china",
          field: "code as value, name as text",
          orderby: "value asc",
          ["step-searh"]: true,
          ["self-field"]: "code",
          ["parent-field"]: "parent_code"
        }),
        h: common_vendor.unref(options).address,
        i: common_vendor.o(($event) => common_vendor.unref(options).address = $event.detail.value),
        j: common_vendor.o((...args) => common_vendor.unref(list) && common_vendor.unref(list)(...args))
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/USER/Desktop/新建文件夹/xiaotuxian/xtx/pages/Address-modules/Address-modules.vue"]]);
wx.createPage(MiniProgramPage);
