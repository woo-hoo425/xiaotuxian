"use strict";
const common_vendor = require("../common/vendor.js");
const baseUrl = "https://pcapi-xiaotuxian-front-devtest.itheima.net";
let request = (params) => {
  let url = baseUrl + params.url;
  let method = params.method || "get";
  let data = params.data || {};
  let header = {};
  if (method == "post") {
    header = {
      "Content-Type": "application/json"
    };
  }
  if (common_vendor.index.getStorageSync("token")) {
    header["Authorization"] = "Bearer " + common_vendor.index.getStorageSync("token");
  }
  return new Promise((resolve, reject) => {
    common_vendor.index.request({
      url,
      method,
      data,
      header,
      success: (res) => {
        let code = res.statusCode;
        if (code == 200) {
          resolve(res.data);
        } else {
          common_vendor.index.clearStorageSync();
          switch (code) {
            case 401:
              common_vendor.index.showModal({
                title: "提示",
                content: "请登录",
                showCancel: false,
                success: (res2) => {
                  if (res2.confirm) {
                    console.log("用户点击确定");
                    common_vendor.index.navigateTo({
                      url: "/pages/login/login"
                    });
                  } else if (res2.cancel) {
                    common_vendor.index.showToast({
                      title: "用户点击取消",
                      duration: 2e3
                    });
                  }
                }
              });
              break;
            case 404:
              common_vendor.index.showToast({
                title: "请求地址不存在"
              });
              break;
            default:
              common_vendor.index.showToast({
                title: "请重试..."
              });
          }
        }
      },
      fail: (err) => {
        reject(err);
      },
      complete: () => {
        common_vendor.index.hideLoading();
        common_vendor.index.hideToast();
      }
    });
  });
};
exports.request = request;
