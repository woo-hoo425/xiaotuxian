"use strict";
const request_index = require("../index.js");
let getSwiperData = (distributionSite) => {
  return request_index.request({
    method: "GET",
    url: "/home/banner",
    data: { distributionSite }
  });
};
let getCategoreData = () => {
  return request_index.request({
    method: "GET",
    url: "/home/category/mutli"
  });
};
let getGuessLikeData = () => {
  return request_index.request({
    method: "GET",
    url: "/home/goods/guessLike"
  });
};
let getMutliData = () => {
  return request_index.request({
    method: "GET",
    url: "/home/hot/mutli"
  });
};
exports.getCategoreData = getCategoreData;
exports.getGuessLikeData = getGuessLikeData;
exports.getMutliData = getMutliData;
exports.getSwiperData = getSwiperData;
