"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  _easycom_uni_swiper_dot2();
}
const _easycom_uni_swiper_dot = () => "../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
if (!Math) {
  _easycom_uni_swiper_dot();
}
const _sfc_main = {
  __name: "swipers",
  props: {
    bannerList: {
      type: Array,
      default: () => []
    }
  },
  setup(__props) {
    common_vendor.ref(true);
    common_vendor.ref(true);
    let interval = common_vendor.ref(2e3);
    let duration = common_vendor.ref(500);
    let dotsStyles = common_vendor.ref({
      backgroundColor: "#f3f3f3",
      selectedBackgroundColor: "#fff"
    });
    let current = common_vendor.ref(0);
    let change = (e) => {
      current.value = e.detail.current;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(__props.bannerList, (item, k0, i0) => {
          return {
            a: item.imgUrl,
            b: item.id
          };
        }),
        b: common_vendor.unref(interval),
        c: common_vendor.unref(duration),
        d: common_vendor.o((...args) => common_vendor.unref(change) && common_vendor.unref(change)(...args)),
        e: common_vendor.p({
          info: __props.bannerList,
          current: common_vendor.unref(current),
          field: "content",
          dotsStyles: common_vendor.unref(dotsStyles)
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/components/swipers/swipers.vue"]]);
wx.createComponent(Component);
