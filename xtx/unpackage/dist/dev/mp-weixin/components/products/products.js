"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "products",
  props: {
    list: {
      tyoe: Object,
      default: () => {
      }
    }
  },
  emits: ["toDetail"],
  setup(__props, { emit: $emit }) {
    let goDetail = () => {
      console.log(111);
      common_vendor.index.navigateTo({
        url: "/pages/detail/detail"
      });
    };
    return (_ctx, _cache) => {
      return {
        a: __props.list.picture,
        b: common_vendor.t(__props.list.name),
        c: common_vendor.t(__props.list.price),
        d: common_vendor.o((...args) => common_vendor.unref(goDetail) && common_vendor.unref(goDetail)(...args))
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-84cc0406"], ["__file", "/Users/kangyuting/study/uni-app/smallRabbit/xiaotuxian/xtx/components/products/products.vue"]]);
wx.createComponent(Component);
