"use strict";
const utils_http = require("../http.js");
let addAddress = (params) => {
  return utils_http.request({
    url: "/member/address",
    method: "POST",
    data: params
  });
};
exports.addAddress = addAddress;
