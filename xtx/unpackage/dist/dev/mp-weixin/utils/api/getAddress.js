"use strict";
const utils_http = require("../http.js");
let getAddress = (params) => {
  return utils_http.request({
    url: "/member/address",
    method: "GET"
  });
};
exports.getAddress = getAddress;
