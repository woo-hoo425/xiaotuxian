"use strict";
const utils_http = require("../http.js");
let login = (phoneNumber) => {
  return utils_http.request({
    method: "POST",
    url: "/login/wxMin/simple",
    data: { phoneNumber }
  });
};
exports.login = login;
