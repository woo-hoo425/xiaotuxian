"use strict";
const common_vendor = require("../common/vendor.js");
const base_url = "https://pcapi-xiaotuxian-front-devtest.itheima.net";
getApp();
let request = (params) => {
  let url = base_url + params.url;
  let method = params.method || "get";
  let data = params.data || {};
  let header = {};
  if (method == "post") {
    header = {
      "Content-Type": "application/json"
    };
  }
  if (common_vendor.index.getStorageSync("token")) {
    header["Authorization"] = common_vendor.index.getStorageSync("token");
  }
  return new Promise((resolve, reject) => {
    common_vendor.index.request({
      url,
      method,
      data,
      header,
      success: (res) => {
        let code = res.statusCode;
        if (code == 200) {
          resolve(res.data);
        } else {
          common_vendor.index.clearStorageSync();
          switch (code) {
            case 401:
              common_vendor.index.showModal({
                title: "提示",
                content: "请登录",
                success: function(res2) {
                  if (res2.confirm) {
                    common_vendor.index.navigateTo({
                      url: "/pages/login/login"
                    });
                  } else if (res2.cancel) {
                    common_vendor.index.showToast({
                      title: "用户点击取消"
                    });
                  }
                }
              });
              break;
            case 404:
              common_vendor.index.showToast({
                title: "请求地址不存在"
              });
              break;
            default:
              common_vendor.index.showToast({
                title: "请重试……"
              });
          }
        }
      },
      fail: (err) => {
        reject(err);
      },
      complete: () => {
        common_vendor.index.hideLoading();
        common_vendor.index.hideToast();
      }
    });
  });
};
exports.request = request;
