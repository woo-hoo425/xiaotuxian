import {request} from "../http.js"
export let addAddress=(params)=>{
	return request({
		url:'/member/address',
		method:'POST',
		data:params
	})
}