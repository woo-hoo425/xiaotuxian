//1、封装基础地址
//2、处理参数
//3、加全局功能如 loading
//4、状态码的判断，并进行相应逻辑处理
//5、请求头的设置
// 全局请求封装
//1、定义基础地址
	const base_url = 'https://pcapi-xiaotuxian-front-devtest.itheima.net'
	let app = getApp()//全局变量
	export let request = (params) => {
	let url = base_url + params.url;
	let method = params.method || "get";
	let data = params.data || {}
	let header = {}
	if (method == "post") {
		header = {
			'Content-Type': 'application/json'
		};
	}
	// token的判断(本地存储)
	if (uni.getStorageSync("token")) {
		header['Authorization'] =uni.getStorageSync("token");
	}
	// if(app?.globalData?.token){
	// 	header['Authorization']=app.globalData.token;
	// }
	// 抛出 promise
	return new Promise((resolve, reject) => {
		uni.request({
			url,
			method,
			data,
			header,
			success: res => {
				let code = res.statusCode
				if (code == 200) {
					resolve(res.data)
				} else {
					uni.clearStorageSync()
					switch (code) {
						case 401:
							uni.showModal({
								title: '提示',
								content: '请登录',
								success: function(res) {
									if (res.confirm) {
										uni.navigateTo({
											url: '/pages/login/login'
										})

									} else if (res.cancel) {
										uni.showToast({
											title: '用户点击取消'
										});
									}
								}
							});
							break;
						case 404:
							uni.showToast({
								title: '请求地址不存在'
							})
							break;
						default:
							uni.showToast({
								title: '请重试……'
							})
					}
				}
			},
			fail: (err) => {
				reject(err)
			},
			complete: () => {
				uni.hideLoading()
				uni.hideToast()
			}
		})
	})

}



/* 
考虑问题：现在小兔鲜对应的服务只有一个 axios.create
如果小兔鲜是多个服务器
*/