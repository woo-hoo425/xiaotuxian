import { request } from "../index";

// 首页数据
// 轮播图的数据
export let getSwiperData = (distributionSite:any)=>{
	return request({
		method:'GET',
		url:'/home/banner',
		data:{distributionSite}
	})
}

// 前台分类
export let getCategoreData = ()=>{
	return request({
		method:'GET',
		url:'/home/category/mutli'
	})
}

// 特惠推荐
export let getPreferenceData = (pageSize:any)=>{
	return request({
		method:'GET',
		url:'/hot/preference',
		data:{
			pageSize
		}
	})
}

//  爆款推荐
export let getInVogueData = (pageSize:any)=>{
	return request({
		method:'GET',
		url:'/hot/inVogue',
		data:{
			pageSize
		}
	})
}

// 一站买全
export let getOneStopData = (pageSize:any)=>{
	return request({
		method:'GET',
		url:'/hot/oneStop',
		data:{
			pageSize
		}
	})
}

// 猜你喜欢
export let getGuessLikeData = ()=>{
	return request({
		method:'GET',
		url:'/home/goods/guessLike'
	})
}

// 首页-热门推荐
export let getMutliData = ()=>{
	return request({
		method:"GET",
		url:'/home/hot/mutli'
	})
}