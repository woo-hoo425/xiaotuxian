// 引入request文件
import {request} from "../index"
var myHeaders = new Headers();
export let getList = (params)=>{
	return request({
		url:"/category/top",
		method:"GET"
	})
}
export let getGoods=(params)=>{
	return request({
		url:"/goods?id="+ params.url,
		method:"GET"
	})
}
export let getGwc=(params)=>{
	return request({
		url:'/member/cart',
		headers: myHeaders,
		method:"GET",
	})
}
